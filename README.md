# buutti-week5



## Getting started

This repository contains buutti homeworks made with docker, azure container app, azure funcrtions, azure app service.

# Homeworks
1. First httpTrigger Azure function:
    https://rafazhttptriggerfunction.azurewebsites.net/api/rafazhttptriggerfunction

2. Continous development:
    https://cd-rafflication.azurewebsites.net/

3. Chained functions
    1st:    
     https://raf-first-chained-function.azurewebsites.net/api/raf-first-chained-function
    2nd:   
     https://raf-second-chained-function.azurewebsites.net/api/raf-second-chained-function